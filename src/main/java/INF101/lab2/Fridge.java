package INF101.lab2;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    int items;
    int fridgeSize = 20;
    List<FridgeItem> fridgeItems = new ArrayList<>();
    
    public int nItemsInFridge() {
        return this.fridgeItems.size();
    }

    public int totalSize() {
        return this.fridgeSize;
    }

    public boolean placeIn(FridgeItem item) {
        if (fridgeItems.size() >= 20) return false;
        this.fridgeItems.add(item);
        if (fridgeItems.contains(item)) return true;
        return false;
    }

    public void takeOut(FridgeItem item) {
        if (!fridgeItems.contains(item)) {
            throw new NoSuchElementException();
        } else {
            this.fridgeItems.remove(item);
        }
    }

    public void emptyFridge() {
        fridgeItems.clear();
    }

    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem items : fridgeItems) {
            if (items.hasExpired()) expiredItems.add(items);
        }
        fridgeItems.removeAll(expiredItems);
        return expiredItems;
    }

}